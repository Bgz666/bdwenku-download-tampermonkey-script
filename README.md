<div id="additional-info" class="user-content" lang="zh-CN">
    <h1>BDwenku Download Tampermonkey script <a href="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script" title="Gitee" rel="nofollow" class="link-instanted" target="_blank"><img src="https://palerock.cn/api-provider/files/view?identity=L2FydGljbGUvaW1hZ2UvMjAyMDA2MjkxNTQyMTMwNzVXcWZyU2dTbC5wbmc=&amp;w=20" alt="gitee.png"></a></h1>
<p><img src="https://palerock.cn/node-service/images/greasyfork/views-info/439850" alt="Daily Installs"><img src="https://palerock.cn/node-service/images/greasyfork/stats/daily-installs/439850" alt="Daily Installs"><img src="https://palerock.cn/node-service/images/greasyfork/stats/daily-updates/439850" alt="Daily Updates"><img src="https://palerock.cn/node-service/images/greasyfork/stats/total-installs/439850" alt="Total Installs"></p>
<hr>
<blockquote>
<p><strong><em>请确保已安装 <code>Tampermonkey 正式版</code> 扩展（脚本管理器）后，再去点击上面的 [安装脚本] ！</em></strong><br>
<em><strong>注意</strong>：使用 <strong>Safari 浏览器</strong>时，可能会遇到部分网站<strong>无法运行</strong>任何油猴脚本，具体原因请见：<a href="https://greasyfork.org/zh-CN/scripts/419081/discussions/110202"><strong>#110202</strong></a></em></p>
<!-- <em>有反馈说火狐浏览器无效，近期没时间修复。大家目前尽量还是用Chromium内核浏览器如Edge，Chrome等食用脚本吧！</em> -->
</blockquote>
<hr>


<h2>暂时失效，请等待解决。急需请使用这位大佬的脚本<a href = "https://greasyfork.org/zh-CN/scripts/435884-wenku-doc-downloader">Wenku Doc Downloader</a></h2>
<h2>Tip:请仔细阅读此文档，有部分问题的解决方法</h2>
<hr>

<h3>强烈推荐食用方法：</h3>
<strong><i>点击《免费下载》---跳转新页面后点《菜单》<a href="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/issues/I4TW6G"> 菜单在哪打开？</a>---（测试）无边距打印---之后再点击《免费下载》</i></strong>

<h3> 注：《页数过多时》，请先下拉滚动条加载一下，确保脚本执行时页面《都曾加载过》。 </h3>
<h4>（拉住滚动条迅速过一遍，看到空白的就等一下加载出来，页数少就不需要）</h4>
<hr>
<p><strong> 更新说明 </strong>  </p>
<p>v1.43 优化<i><strong> 超过50页文档打开方式，改为菜单点击<a href="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/issues/I4TW6G"> 菜单在哪打开？</a>一键打开剩余页面 </strong></i></p>
<p>v1.42 优化<i><strong> 配置默认取消标题，边框 </strong></i></p>
<p>v1.41 新增<i><strong> 超过50页的文档自动打开 </strong></i></p>
<p>V1.38 适配<i><strong> link后缀/tfview后缀的文库链接</strong></i></p>
<p>V1.35 增加菜单<i><strong>（测试）无边距打印</strong></i><a href="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/issues/I4TW6G"> 菜单在哪打开？</a></p>
<!-- <p>V1.32 自定义高度栏<i><strong>立即显示</strong></i>，优化<i><strong>打印/下载预览的显示效果</strong></i></p> -->
<p>V1.27 修复bug<i><strong>页面重复</strong></i></p>
<p>V1.23 增加菜单<i><strong>取消《打印时文档每页必在不同页》</strong></i></p>
<p>V1.22 增加菜单<i><strong>自定义页面高度</strong></i> ,尝试解决页面断裂<a href="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/issues/I4TW6G"> 菜单在哪打开？</a></p>
<p>V1.21 增加菜单<i><strong>去除/恢复标题，去除/恢复边框 </strong></i></p>
<p>V1.20 修改<i><strong>常见问题 </strong></i>的网址跳转，去除对解决问题无效的问题网址</p>
<p>V1.17 修复<i><strong>部分</strong></i>文档断裂问题</p>

<hr>
<h2>目录</h2>
<h4>一.介绍</h4>
<h4>二.适用文档范围</h4>
<h4>三.使用方法</h4>
<h4>四.部分问题及解决</h4>

<hr>
<h2>一.介绍</h2>

<p><strong>1.某度文库下载脚本，配合油猴脚本插件（Tampermonkey）使用</strong></p>
<p><strong>2.下载的pdf，文字可以复制</strong></p>
<p><strong>3.完全本地化实现，非网址解析</strong></p>
<p><strong>4.脚本仅供学习交流使用，请勿用于商业用途</strong></p>



<hr>
<h2>二.适用文档范围</h2>

<h3>A.本脚本适用情况：</h3>

<p>1.有<strong><em> 继续阅读 </em></strong> 字样的文档<br>
包括<strong> 部分付费文档 </strong>和<strong> VIP文档 </strong><br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E7%BB%A7%E7%BB%AD%E9%98%85%E8%AF%BB.png" alt="图1">   </p>

<p>2.<strong>试读页面</strong>即是文档的<strong>全部页面</strong><br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E9%80%82%E7%94%A8/%E9%80%82%E7%94%A83.png" alt="图1">   </p>

<h3>B.本脚本不适用情况：</h3>

<p><strong>1.试读页面非全部页面</strong><br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E9%80%82%E7%94%A8/%E4%B8%8D%E9%80%82%E7%94%A81.png" alt="图1"><br>
<strong>2.下载后阅读全文</strong><br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E9%80%82%E7%94%A8/%E4%B8%8D%E9%80%82%E7%94%A82.png" alt="图1">    </p>
<hr>
<h2>三.使用方法</h2>

<h4>1.打开页面等待加载完成，点击按钮</h4>

<p><img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E5%9B%BE1.png" alt="图1">  </p>

<h4>2.等待新页面加载完成，再次点击按钮。</h4>

<h4>如果未打开新页面，请允许弹出窗口。</h4>

<p><img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E5%9B%BE2.png" alt="图2"></p>

<h4>3.等待此页面出现，点击保存</h4>

<p><img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E5%9B%BE3.png" alt="图3">    </p>

<hr>
<h2>四.问题</h2>
<h3 id='issue'>提示：1.页数过多可能打印页面加载慢，请耐心等待
2.如果有任何显示不全，图片缺失，页面空白或文字错位.如下图问题</h3>

<h4>显示不全</h4>

<p>阅读时<br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E9%97%AE%E9%A2%98%7Eissues/%E9%98%85%E8%AF%BB%E6%97%B6.png" alt="图4"><br>
下载时<br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E9%97%AE%E9%A2%98%7Eissues/%E6%89%93%E5%8D%B0%E6%97%B6.png" alt="图4"></p>

<h4>图片缺失</h4>

<p><img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E9%97%AE%E9%A2%98%7Eissues/%E5%9B%BE4.png" alt="图4">  </p>

<h4>文字错位</h4>

<p><img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E9%97%AE%E9%A2%98%7Eissues/%E5%9B%BE2.png" alt="图2">  </p>

<h2>请按如下操作解决，如果如下操作后还为空白，请刷新页面，重新执行一次脚本</h2>

<p><img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E8%83%8C%E6%99%AF%E5%9B%BE%E5%9E%8B1.png" alt="图1"><br>
<img src="https://gitee.com/Bgz666/bdwenku-download-tampermonkey-script/raw/master/%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/%E8%83%8C%E6%99%AF%E5%9B%BE%E5%BD%A22.png" alt="图1">  </p>
<h4>目前仍存在的问题</h4>

<h4>暂无，如果发现问题，请反馈。</h4>

    
  </div>